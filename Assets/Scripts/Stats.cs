﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats {

	//Esta es nuestra clase base, de aquí es donde definiremos los stats del enemigo y del player

	public float LifePoints; 
	public float Strength; 
	public float Resistance; 
	public float Agility;
	public float Magic; 


}
